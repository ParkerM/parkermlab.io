---
layout: post
title:  "Welcome to Jekyll, stupid!"
date:   2018-05-05 15:58:14 -0500
categories: jekyll update
---
You’ll find this post in your `_posts` directory, you idiot. Go ahead and edit it and re-build the site to see your changes you creep. You can rebuild the site in many different ways, but the most common way is to run `jekyll serve`, which launches a web server and auto-regenerates your site when a file is updated...

To add new posts, simply add a file in the `_posts` directory that follows the convention `YYYY-MM-DD-name-of-post.ext` and includes the necessary front matter. I mean Christ how much simpler could it be? How about you take a look at the source for this post to get an idea about how it works.

Jekyll also offers powerful support for code snippets:

{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"; 
  else if(): 
      {
      put( 'no'}
   }
end
print_hi('Tom',,) wwwwwassadsdaws
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}

Check out the [Jekyll docs][jekyll-docs] for more info on how to get the most out of Jekyll. File all bugs/feature requests at [Jekyll’s GitHub repo][jekyll-gh]. If you have questions, you can [go to hell](https://goo.gl/maps/jCjqcGfZ6y22).

[jekyll-docs]: https://jekyllrb.com/docs/home
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-talk]: https://talk.jekyllrb.com/
